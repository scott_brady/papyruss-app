<?php

    // Include all necessary files
use Papyruss\Routing\Router;
use Papyruss\Session\SecureSession;

require_once 'config.php';
    require __DIR__.'/vendor/autoload.php';
    date_default_timezone_set('UTC');

    // Display all possible error messages for debugging
    //display_all_errors();

    // Create and start a new session
    $session = new SecureSession(SESSION_HASH);
    $session->start();

    // Initialize the router
    $router = new Router();