<?php include('master/head.php'); ?>

    <div class="container-fluid login-container clearfix">
        <div class="register-card">
            <img src="/assets/images/logo_light.png" />
            <h1>Content Trees</h1>
            <em><a href="/index">Go Home</a></em>
            <h3>Register User</h3>
            <em>All fields are required</em>
            <?php
                if (! isset($_POST)) {
                    $_POST = [];
                }
                if (isset($errors_msg))
                {
                    echo '<div class="form-error">';

                    echo '<h4>Please correct the following error';
                    echo (count($errors_msg) > 1) ? 's:' :  ':';
                    echo '</h4>';

                    echo '<ul>';

                    foreach($errors_msg as $error_msg) {
                        echo '<li>'. $error_msg . '</li>';
                    }

                    echo '</ul></div>';
                }

                if (isset($_POST))
                {

                }
            ?>

            <form action="/register/post" method="post" id="login-form">
                <div class="login-form" id="register-form">

                    <h3>Is this a primary user?</h3>
                    <ul class="register-radio">
                        <li>
                            <input type="radio" id="y-option" name="primary" value="yes" <?php fillData('primary', 'radio', 'yes'); ?> >
                            <label class="radio-label" for="y-option">Yes</label>
                            <div class="check"></div>
                        </li>

                        <li>
                            <input type="radio" id="n-option" name="primary" value="no"<?php fillData($_POST['primary'], 'radio', 'no'); ?> >
                            <label class="radio-label" for="n-option">No</label>
                            <div class="check"></div>
                        </li>
                    </ul>



                    <div class="register-select" id="user-type">
                        <label for="user-type" style="display:none">Select User Type:</label>
                        <select name="user-type" id="user-type" required>
                            <option value="" disabled <?php fillData('user-type', 'select', ''); ?>>Choose the user type</option>
                            <option value="1" <?php fillData('user-type', 'select', 1); ?> >Option 1</option>
                            <option value="2" <?php fillData('user-type', 'select', 2); ?>>Option 2</option>
                            <option value="3" <?php fillData('user-type', 'select', 3); ?>>Option 3</option>
                        </select>
                    </div>

                    <div class="row">
                        <div class="input-container input-half">
                            <input type="text" name="first-name" id="first-name" required value="<?php fillData('first-name') ?>" />
                            <label for="first-name">First Name:</label>
                            <div class="login-bar"></div>
                        </div>
                        <div class="input-container input-half">
                            <input type="text" name="last-name" id="last-name" required value="<?php fillData('last-name') ?>" />
                            <label for="last-name">Last Name:</label>
                            <div class="login-bar"></div>
                        </div>
                    </div>

                    <div class="input-container">
                        <input type="tel" name="phone-number" id="phone-number" maxlength="14" required value="<?php fillData('phone-number') ?>" />
                        <label for="phone-number">Phone:</label>
                        <div class="login-bar"></div>
                    </div>

                    <div class="input-container <?php echo isset($errors['email']) ? 'form-error' : ''; ?>">
                        <input type="text" name="email" id="email" required value="<?php fillData('email') ?>" />
                        <label for="email">Email:</label>
                        <div class="login-bar"></div>
                    </div>

                    <div class="input-container <?php echo isset($errors['conf_password']) ? 'form-error' : ''; ?>">
                        <input type="password" name="password" id="password" required value="<?php fillData('password') ?>" />
                        <label for="password">Password:</label>
                        <div class="login-bar"></div>
                    </div>

                    <div class="input-container <?php echo isset($errors['conf_password']) ? 'form-error' : ''; ?>">
                        <input type="password" name="conf-password" id="conf-password" required value="<?php fillData('conf-password') ?>" />
                        <label for="conf-password">Confirm Password:</label>
                        <div class="login-bar"></div>
                    </div>

                    <div class="register-select">
                        <label for="user-account" style="display:none">Select Account:</label>
                        <select name="user-account" id="user-account" required>
                            <option value="" disabled <?php fillData('user-account', 'select', null); ?>>Choose account</option>
                            <option value="0" <?php fillData('user-account', 'select', 0); ?>>Create a new account</option>
                            <?php foreach ($accounts as $account) { ?>
                                <option value="<?=$account['id']?>" <?php fillData('user-account', 'select', $account['id']); ?>><?= $account['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="create-account" id="create-account" style="display: none;">
                        <h3>Register Account</h3>
                        <em>All fields are required for a new account</em>
                        <div class="input-container">
                            <input type="text" name="acc-name" id="acc-name" value="<?php fillData('acc-name') ?>"/>
                            <label for="acc-name">Account Name:</label>
                            <div class="login-bar"></div>
                        </div>

                        <div class="input-container">
                            <input type="text" name="acc-website" id="acc-website" value="<?php fillData('acc-website') ?>"/>
                            <label for="acc-website">Website:</label>
                            <div class="login-bar"></div>
                        </div>

                        <div class="account-select">
                            <div class="account-select" id="account-select">
                                <label for="account-type" style="display:none">Select Account:</label>
                                <select name="account-type" id="account-type">
                                    <option value="" disabled <?php fillData('account-type', 'select', ''); ?>>Account Type</option>
                                    <option value="1" <?php fillData('account-type', 'select', 1); ?>>1</option>
                                    <option value="2" <?php fillData('account-type', 'select', 2); ?>>2</option>
                                    <option value="3" <?php fillData('account-type', 'select', 3); ?>>3</option>
                                </select>
                            </div>
                            <div class="account-select" id="market-select">
                                <label for="market-type" style="display:none">Select Account:</label>
                                <select name="market-type" id="market-type">
                                    <option value="" disabled <?php fillData('market-type', 'select', ''); ?>>Market Type</option>
                                    <option value="1" <?php fillData('market-type', 'select', 1); ?>>1</option>
                                    <option value="2" <?php fillData('market-type', 'select', 2); ?>>2</option>
                                    <option value="3" <?php fillData('market-type', 'select', 3); ?>>3</option>
                                </select>
                            </div>
                            <div class="account-select" id="audience-select">
                                <label for="audience-type" style="display:none">Select Account:</label>
                                <select name="audience-type" id="audience-type">
                                    <option value="" disabled <?php fillData('audience-type', 'select', ''); ?>>Audience Type </option>
                                    <option value="1" <?php fillData('audience-type', 'select', 1); ?>>1</option>
                                    <option value="2" <?php fillData('audience-type', 'select', 2); ?>>2</option>
                                    <option value="3" <?php fillData('audience-type', 'select', 3); ?>>3</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <input type="hidden" name="acc-id" value="<?= $new_id; ?>" />
                    <input type="hidden" name="csrf-token" value="<?= $csrf_token; ?>" />

                    <button type="submit" class="btn btn-submit">Register</button>
                </div>



            </form>
        </div>
    </div>

<?php include('master/footer.php'); ?>