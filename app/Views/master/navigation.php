<div id="wrapper" class="active">

    <!-- Sidebar -->
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul id="sidebar-menu" class="sidebar-nav">
            <li class="sidebar-brand">
                <a id="menu-toggle" href="#"><span class="sidebar-icon"><img src="/assets/images/logo_light.png" alt="Content Trees Logo" /></span>Content Trees
                </a>
            </li>
        </ul>
        <ul class="sidebar-nav" id="sidebar">
            <li><a href="#" class="active"><span class="sidebar-icon"><i class="fa fa-tachometer fa-2x" aria-hidden="true"></i></span>Dashboard</a></li>
            <li><a href="#"><span class="sidebar-icon"><i class="fa fa-pie-chart fa-2x" aria-hidden="true"></i></span>Reporting</a></li>
            <li><a href="#"><span class="sidebar-icon"><i class="fa fa-comments fa-2x" aria-hidden="true"></i></span>Social Networks</a></li>
            <li><a href="#"><span class="sidebar-icon"><i class="fa fa-cog fa-2x" aria-hidden="true"></i></span>Account Settings</a></li>
            <li><a href="#"><span class="sidebar-icon"><i class="fa fa-power-off fa-2x" aria-hidden="true"></i></span>Logout</a></li>
        </ul>
    </div> <!-- ./sidebar-wrapper -->

    <!-- Page content -->
    <div id="page-content-wrapper">

        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">

            <div class="container-fliud">
                <div class="navbar-header">
                    <button class="opened side-nav-toggle">
                        <span class="bar-top"></span>
                        <span class="bar-mid"></span>
                        <span class="bar-bot"></span>
                    </button>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">
                                <i class="fa fa-user fa-2x nav-popup-toggle" aria-hidden="true"></i>
                            </a>
                        </li>

                        <li>
                            <div class="nav-popup-toggle dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o fa-2x nav-popup-toggle" aria-hidden="true"></i><span class="notification-badge">3</span>
                            </div>
                            <div class="nav-popup dropdown-menu" id="notifications">
                                <div class="nav-popup-header">NOTIFICATIONS</div>
                                <div class="nav-popup-body">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                <strong>A new post is ready</strong>
                                                <em>2 hours left to post</em>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                <strong>A new post is ready</strong>
                                                <em>2 hours left to post</em>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="page-content inset">