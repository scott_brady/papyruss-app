<?php include('master/head.php'); ?>

<div class="container-fluid login-container">
    <div class="login-card">
        <img src="/assets/images/logo_light.png" />
        <h1>Content Trees</h1>
        <em><a href="/index">Go Home</a></em>
        <h3>Login</h3>
        <?php
        if (isset($errors_msg))
        {
            echo '<div class="form-error">';

            echo '<h4>Please correct the following error';
            echo (count($errors_msg) > 1) ? 's:' :  ':';
            echo '</h4>';

            echo '<ul>';

            foreach($errors_msg as $error_msg) {
                echo '<li>'. $error_msg . '</li>';
            }

            echo '</ul></div>';
        }
        ?>
        <form action="/login/post" method="post" id="login-form">
            <div class="email-login">
                <div class="input-container <?php echo isset($errors['login']) ? 'form-error' : ''; ?>">
                    <input type="text" name="email" id="email" required value="<?php fillData('email') ?>" />
                    <label for="username">Email:</label>
                    <div class="login-bar"></div>
                </div>
                <div class="input-container <?php echo isset($errors['login']) ? 'form-error' : ''; ?>">
                    <input type="password" name="password" id="password" required value="<?php fillData('password') ?>"/>
                    <label for="password">Password:</label>
                    <div class="login-bar"></div>
                </div>
                <button type="submit" class="btn btn-submit">Login</button>
            </div>
        </form>
</div>

<?php include('master/footer.php'); ?>