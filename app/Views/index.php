<?php include('master/head.php'); ?>

    <div class="container-fluid login-container">
    <div class="login-card">
        <img src="/assets/images/logo_light.png" />
        <h1>Content Trees</h1>
        <h3>You are not logged in</h3>
        <hr>
        <h4><a href="/register">REGISTER</a> / <a href="/login">LOGIN</a></h4>
    </div>

<?php include('master/footer.php'); ?>