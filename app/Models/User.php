<?php
/**
 * File: User.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@rxinsider.com>
 *
 * Database layer for the User model. Extends the base model.
 */
namespace App\Models;


use Papyruss\Database\Model;
use PDOException;
use Papyruss\Session\SecureSession;

class User extends Model
    {
        public $table = "users";

        public $hash_options = array("cost" => 12);
        public $locked_options = array(
            "max_failures" => 10,
            "failure_time" => 600
        );

        protected $session;


        public function __construct()
        {
            $this->session = new SecureSession(SESSION_HASH);
            parent::__construct();
        }

        /**
         * @param $user_data
         * @param string $acc_data
         * @return bool
         *
         * Register the user in the database
         */
        public function registerUser($user_data, $acc_data = "")
        {
            try {
                // Only add users with a unique email address
                $user_exists = $this->getUserByEmail($user_data['email']);

                if (!$user_exists) {
                    $data = array(
                        'user_type_id'      => $user_data['user_type_id'],
                        'account_id'        => $user_data['account_id'],
                        'primary_contact'   => $user_data['primary_contact'],
                        'first_name'        => $user_data['first_name'],
                        'last_name'         => $user_data['last_name'],
                        'phone'             => $user_data['phone'],
                        'remember_token'    => '',
                        'email'             => $user_data['email'],
                        'password'          => password_hash($user_data['password'], PASSWORD_DEFAULT, $this->hash_options),
                        'created_at'        => date('Y-m-d H:i:s')
                    );

                    // Check to see if an acocunt is also being created
                    if ($acc_data) {
                        $account = array(
                            'name'              => $acc_data['account_name'],
                            'account_type_id'   => $acc_data['account_type']
                        );
                        if (isset($acc_data['account_website'])) {
                            $account["website"] = $acc_data['account_website'];
                        }
                        if (isset($acc_data['market_type'])) {
                            $account["market_type_id"] = $acc_data['market_type'];
                        }
                        if (isset($acc_data['audience_type_id'])) {
                            $account["audience_type_id"] = $acc_data['audience_type'];
                        }

                        // Create the new account
                        $this->insert('accounts', $account);
                    }

                    // Create the new user
                    $this->insert($this->table, $data);

                    return true;

                } else {
                    // If the email address is not unqiue, display an error and the registeration form
                    array_push($errors_msg, 'That email has already been registered');
                    $errors['email'] = 1;
                    include(BASEPATH . '/app/views/register.php');

                    return false;
                }
            } catch (PDOException $e) {
                echo $e->getMessage();
                die();
            }
        }

        /**
         * @param $email
         * @return bool|object
         *
         * Attempts to get a user based on the given email address.
         */
        public function getUserByEmail($email)
        {
            $bind[":email"] = $email;

            $result = $this->select($this->table, "email = :email", $bind);
            if ($result) {
                return $result;
            }
            return false;
        }

        /**
         * @param $email
         * @param $password
         * @return bool
         *
         * Login the user if the password matches. If the password is incorrect, start counting down login attempts, if too many failed attempts lock the account
         */
        public function loginUser($user, $email, $password)
        {
            if (password_verify($password, $user['password'])) {
                // if PASSWORD_DEFAULT changes, update password encrypting alg
                if (password_needs_rehash($user['password'], PASSWORD_DEFAULT, $this->hash_options)) {
                    $this->updatePassword($email, $password);
                }

                // Create any sessions needed
                $payload = array(
                    'user_type' => $user['user_type_id'],
                    'account_id' => $user['account_id'],
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'last_logged' => $user['updated_at']
                );

                $payload = json_encode($payload);

                $data = array(
                    "id" => $this->session->getId(),
                    "user_id" => $user['id'],
                    "ip_address" => $_SERVER['REMOTE_ADDR'],
                    "user_agent" => $_SERVER['HTTP_USER_AGENT'],
                    "payload" => $payload,
                    "created_at" => date('Y-m-d H:i:s')
                );

                $this->session->set('id',           $data['id']);
                $this->session->set('user_id',      $data['user_id']);
                $this->session->set('ip_address',   $data['ip_address']);
                $this->session->set('user_agent',   $data['user_agent']);
                $this->session->set('payload',      $data['payload']);
                $this->session->set('created_at',   $data['created_at']);

                // Add the session information into the database
                $this->addSession($data);

                $this->touch($this->table, $user['id']);

                return true;
            }
            return false;
        }

        /**
         * @param $id
         *
         * Lock the account if there are too many failed login attempts in the set amount of time
         */
        public function lockAccount($id)
        {
            $update_info = array('locked' => 1);
            $bind = array(':locked' => 1);

            $this->update($this->table, $update_info, 'id = ' . $id, $bind);
        }

        /**
         * @param $email
         * @param $password
         *
         * Updates the user's password
         */
        public function updatePassword($email, $password)
        {
            $password = password_hash($password, PASSWORD_DEFAULT, $this->hash_options);

            $update_info = array('password' => ':password');
            $update_bind = array(
                ':email' => $email,
                'password' => $password
            );

            $this->update($this->table, $update_info, 'email = :email', $update_bind);
        }

        /**
         * @return bool|int
         *
         * Get all accounts registered in the database
         */
        public function getAllAccounts()
        {
            return $this->select('accounts');
        }

        /**
         * @param $data
         *
         * Add information into the sessions table
         */
        public function addSession($data)
        {
            $this->insert('sessions', $data);
        }
    }