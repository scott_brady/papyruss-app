<?php
/**
 * File: SocialMediaController.php
 * Created on: 7/21/16
 *
 * @author Joseph Gengarella <joseph.gengarella@rxinsider.com>
 *
 * Controller for the functionality related to the user
 */

    namespace App\Controllers;

    use App\Models\User;
    use Hybrid_Auth;
    use Papyruss\Routing\Controller;

    class SocialMediaController extends Controller
    {
        public $user;
        public $hybridauth;

        public function __construct()
        {
            $this->user = new User();
            $this->hybridauth = new Hybrid_Auth(AUTH_PATH);
        }

        /**
         * Connect a user's social media account to their login
         */
        public function connectSocialAccount() {
            $provider = $_REQUEST['provider'];
            try
            {
                $adapter = $this->hybridauth->authenticate($provider);
            } catch (\Exception $e) {
                switch( $e->getCode() ){
                    case 0 : echo "Unspecified error."; break;
                    case 1 : echo "Hybriauth configuration error."; break;
                    case 2 : echo "Provider not properly configured."; break;
                    case 3 : echo "Unknown or disabled provider."; break;
                    case 4 : echo "Missing provider application credentials."; break;
                    case 5 : echo "Authentification failed. "
                        . "The user has canceled the authentication or the provider refused the connection.";
                        break;
                    case 6 : echo "User profile request failed. Most likely the user is not connected "
                        . "to the provider and he should authenticate again.";
                        //$twitter->logout();
                        break;
                    case 7 : echo "User not connected to the provider.";
                        //$twitter->logout();
                        break;
                    case 8 : echo "Provider does not support this feature."; break;
                }

                // well, basically your should not display this to the end user, just give him a hint and move on..
                echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
            }
        }
    }