<?php
/**
 * File: IndexController.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@rxinsider.com>
 *
 * Controller for the index page and other basic page's functionality
 */

    namespace App\Controllers;

    use Papyruss\Routing\Controller;
    use Papyruss\Session\SecureSession;

    class IndexController extends Controller
    {
        protected $session;

        public function __construct()
        {
            $this->session = new SecureSession(SESSION_HASH);
        }

        /**
         * Display the index page
         */
        public function index()
        {
            if (!$this->session->get('user_id')) {
                $this->show('/app/views/index.php');
            } else {
                $this->redirect('/admin');
            }
        }
    }