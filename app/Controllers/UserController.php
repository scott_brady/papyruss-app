<?php
/**
 * File: UserController.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@rxinsider.com>
 *
 * Controller for the functionality related to the user
 */

    namespace App\Controllers;

    use App\Models\User;
    use Papyruss\Routing\Controller;
    use Papyruss\Session\SecureSession;

    class UserController extends Controller
    {
        public $user;
        public $session;

        public function __construct()
        {
            $this->user = new User();
            $this->session = new SecureSession(SESSION_HASH);
        }

        /**
         * Display the register page
         */
        public function register() {
            $this->isLoggedIn($this->session, '/index');

            // Set a csrf token session if one is not set
            if(! $this->session->get('_csrf_token')) {
                $this->session->set('_csrf_token', csrf_generator());
            }

            $csrf_token = $this->session->get('_csrf_token');

            $accounts = $this->user->getAllAccounts();
            if (!isAssoc($accounts)) {
                $accounts = array($accounts);
            }

            // Get the next ID if a new account is being created
            $new_id = $this->user->run('SELECT * FROM accounts ORDER BY id DESC LIMIT 1');
            $new_id = $new_id[0]['id'] + 1;

            $this->show('/app/views/register.php');
        }

        /**
         * Post the entered information into the database and process it
         */
        public function registerPost() {
            $errors     = [];
            $errors_msg = [];

            $user_data['user_type_id']      = htmlspecialchars($_POST['user-type']);
            $user_data['first_name']        = htmlspecialchars($_POST['first-name']);
            $user_data['last_name']         = htmlspecialchars($_POST['last-name']);
            $user_data['phone']             = htmlspecialchars($_POST['phone-number']);
            $user_data['email']             = htmlspecialchars($_POST['email']);
            $user_data['password']          = htmlspecialchars($_POST['password']);
            $user_data['conf_password']     = htmlspecialchars($_POST['conf-password']);
            $user_data['account_id']        = htmlspecialchars($_POST['user-account']);
            $user_data['new_account_id']    = htmlspecialchars($_POST['acc-id']);
            $user_data['primary_contact']   = htmlspecialchars($_POST['primary']);

            $acc_data['account_name']       = htmlspecialchars($_POST['acc-name']);
            $acc_data['account_website']    = htmlspecialchars($_POST['acc-website']);

            // Double check if these fields are filled (not required)
            if (isset($_POST['account-type']))
                $acc_data['account_type']       = htmlspecialchars($_POST['account-type']);
            if (isset($_POST['market-type']))
                $acc_data['market_type']        = htmlspecialchars($_POST['market-type']);
            if (isset($_POST['audience-type']))
                $acc_data['audience_type']      = htmlspecialchars($_POST['audience-type']);

            $csrf_token                     = htmlspecialchars($_POST['csrf-token']);

            // If any required field is left blank, throw an error
            foreach($user_data as $entry)
            {
                if (strlen($entry) === 0) {
                    array_push($errors_msg, 'Please fill out all fields');
                    $errors[$entry] = 1;
                }
            }

            // Check if a new account is being created
            if ($user_data['account_id'] === 0)
            {
                foreach ($acc_data as $entry)
                {
                    if (strlen($entry) === 0 && strlen($errors[0]) === 0) {
                        array_push($errors_msg, 'Please fill out all fields w/ account');
                        $errors[$entry] = 1;
                    }
                }
            }

            // Check email validity
            if (!filter_var($user_data['email'], FILTER_VALIDATE_EMAIL)) {
                array_push($errors_msg, 'Please enter a valid email address');
                $errors['email'] = 1;
            }

            // Check to see if the passwords entered are matching
            if ($user_data['password'] != $user_data['conf_password']) {
                array_push($errors_msg, 'Please make sure your passwords match');
                $errors['conf_password'] = 1;
            }

            // Check the csrf token and match it with the session set
            if (empty($csrf_token)) {
                array_push($errors_msg, 'There was an unexpected error, please try again');
            } else {
                if ( !hash_equals($csrf_token, $this->session->get('_csrf_token'))) {
                    array_push($errors_msg, 'There was an unexpected error, please try again');
                }
            }

            // Check the uniqueness of the email address
            if ($user_exists = $this->user->getUserByEmail($user_data['email'])) {
                array_push($errors_msg, 'That email is already registered');
                $errors['email'] = 1;
            }

            // If there are any errors, display the errors and the registration form
            if (empty($_POST) || ! empty($errors_msg) || ! empty($errors)) {
                $accounts = $this->user->getAllAccounts();
                if (!isAssoc($accounts)) {
                    $accounts = array($accounts);
                }
                include (BASEPATH . '/app/views/register.php');
                die();
            } else {
                // If a new account is being created
                if ($user_data['account_id'] === 0) {
                    $user_data['account_id'] = $user_data['new_account_id'];
                    $this->user->registerUser($user_data, $acc_data);
                } else {
                    $this->user->registerUser($user_data);
                }
            }

            $this->redirect('/index');
        }

        public function login() {
            $this->isLoggedIn($this->session, '/admin');

            $this->show('/app/views/login.php');
        }

        /**
         * Manipulate data that is posted from the login form
         */
        public function loginPost()
        {
            $errors     = [];
            $errors_msg = [];

            $email      = htmlentities($_POST['email']);
            $password   = htmlentities($_POST['password']);

            $userInfo   = $this->user->getUserByEmail($email);

            // If there is a matching email in the database
            if ($userInfo) {
                // If the account is locked, redirect to a locked page
                if ($userInfo['locked'] == 1) {
                    $this->redirect('/locked');
                } else {
                    // Attempt to log in the user with the given credentials
                    if($this->user->loginUser($userInfo, $email, $password)) {
                        $this->redirect('/admin');
                    } else {
                        // If password is wrong, check how many times were failed previously
                        if($userInfo['failed_count'] > $this->user->locked_options["max_failures"])
                        {
                            // Lock the account if login was failed too many times in X minutes
                            $this->user->lockAccount($userInfo['id']);
                        } else if ((time() - strtotime($userInfo['first_failed'])) > $this->user->locked_options["failure_time"]) {
                            // If this is the first failed attempt, set the count to 1 and the time to now
                            $update = array(
                                "first_failed" => date('Y-m-d H:i:s'),
                                "failed_count" => 1
                            );
                            $bind = array(
                                ":first_failed" => date('Y-m-d H:i:s'),
                                ":failed_count" => 1
                            );
                            $this->user->update($this->user->table, $update, 'id='.$userInfo['id'], $bind);
                        } else {
                            // If this is 2-X times failed in the last X minutes, increment failed counter
                            $failed_count = $userInfo['failed_count'] + 1;
                            if ($failed_count <= $this->user->locked_options["max_failures"]) {
                                $update = array(
                                    "failed_count" => $failed_count
                                );
                                $bind = array(
                                    ":failed_count" => $failed_count
                                );
                                $this->user->update($this->user->table, $update, 'id=' . $userInfo['id'], $bind);
                            }
                        }

                        // If the credentials are incorrect, display and error and the login form
                        array_push($errors_msg, 'Username and password did not match');
                        $errors['login'] = 1;
                        include (BASEPATH . '/app/views/login.php');
                    }
                }
            } else {
                // If the credentials are incorrect, display and error and the login form
                array_push($errors_msg, 'Username and password did not match');
                $errors['login'] = 1;
                $this->show('/app/views/login.php');
            }
        }

        /**
         * Log the user out and end all sessions
         */
        public function logout()
        {
            $this->session->end();
            $this->redirect('/index');
        }
    }