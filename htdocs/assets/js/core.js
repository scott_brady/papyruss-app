// Set the size for the smallest breakpoint
const MOBILE_BREAKPOINT = 767;

// Common / global variables / selectors
var $window_size = $(window).width();
var $wrapper = $('#wrapper');
var $sidebar_toggle = $('.side-nav-toggle');
var $sidebar = $('#sidebar-wrapper');
var $nav_popup_toggle = $('.nav-popup-toggle');
var $page_wrapper = $('#page-content-wrapper');
var $user_account = $('#user-account');

// Flag variables
var mouseover_flag = 0;

$(document).ready(function() {

    $sidebar_toggle.on('click', function(e) {
        e.preventDefault();
        $wrapper.toggleClass('active');
    });

    if($window_size <= MOBILE_BREAKPOINT) {
        $wrapper.removeClass('active');
        $sidebar_toggle.removeClass('opened');
    }

    $sidebar_toggle.on('click', function (e) {
        $(this).toggleClass('opened');
    });


    $sidebar.on({
        mouseenter: function() {
            if (!$wrapper.hasClass('active')) {
                $wrapper.addClass('active');
                $sidebar_toggle.addClass('opened');

                mouseover_flag = 1;
            }
        },
        mouseleave: function() {
            if (mouseover_flag === 1) {
                $wrapper.removeClass('active');
                $sidebar_toggle.removeClass('opened');

                mouseover_flag = 0;
            }
        }
    });

    $sidebar.on('click', function() {
        if (mouseover_flag === 1) {
            $wrapper.addClass('active');
            $sidebar_toggle.addClass('opened');

            mouseover_flag = 0;
        }
    });

    // Set the account registration fields to required if they are visible
    $user_account.change(function() {
        var $acc_field      = $('#create-account');
        var $acc_name       = $('#acc-name');
        var $acc_website    = $('#acc-website');
        var $acc_type       = $('#account-type');
        var input_value     = parseInt($(this).val());

        if (input_value == 0) {
            $acc_field.show();
            $acc_name   .attr('required', true);
            $acc_website.attr('required', true);
            $acc_type   .attr('required', true);
        } else {
            $acc_field.hide();
            $acc_name   .removeAttr('required');
            $acc_website.removeAttr('required');
            $acc_type   .removeAttr('required');
        }
    });

    // Set the account registration fields to required if they are visible
    if (parseInt($user_account.val()) == 0) {
        $('#create-account').show();
        $('#acc-name')      .attr('required', true);
        $('#acc-website')   .attr('required', true);
        $('#account-type')  .attr('required', true);
    }

    // Format the phone number on the fly for registration
    $('#phone-number', '#register-form')
        .keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            $phone = $(this);

            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                if ($phone.val().length === 4) {
                    $phone.val($phone.val() + ')');
                }
                if ($phone.val().length === 5) {
                    $phone.val($phone.val() + ' ');
                }
                if ($phone.val().length === 9) {
                    $phone.val($phone.val() + '-');
                }
            }

            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 ||
            key == 9 ||
            key == 46 ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
        })

        .bind('focus click', function () {
            $phone = $(this);

            if ($phone.val().length === 0) {
                $phone.val('(');
            }
            else {
                var val = $phone.val();
                $phone.val('').val(val); // Ensure cursor remains at the end
            }
        })

        .blur(function () {
            $phone = $(this);

            if ($phone.val() === '(') {
                $phone.val('');
            }
        });

});

// Change the style based on the CURRENT window width
$(window).on('resize', function() {
    if (getWindowSize() <= MOBILE_BREAKPOINT) {
        $wrapper.removeClass('active');
        $sidebar_toggle.removeClass('opened');
    }
    if (getWindowSize() > MOBILE_BREAKPOINT) {
        if ($wrapper.hasClass(('active')) === false) {
            $wrapper.addClass('active');
            $sidebar_toggle.addClass('opened');

        }
    }
});

// Gey the window width
function getWindowSize() {
    return $(window).width();
}