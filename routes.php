<?php
/**
 * File: IndexController.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@rxinsider.com>
 *
 * The routes that are added to the Router when a page is loaded.
 *
 * How to use:
 * $router = new Router();
 * $router->add('path/to/something', 'ControllerName@method')
 * $router->add('using/{url}/{params}', 'ControllerName@method')
 *      Params are sent as an array:
 *          using/10/123 returns
 *          Array("url" => 10, "params" => 123)
 */

    /**
     * Index
     */
    // Base landing page (for both / and /index)
    $router->add('/', 'IndexController@index');
    $router->add('/index', 'IndexController@index');

    /**
     * Authentication
     */
    // Registration page (both for the form and for posting)
    $router->add('/register', 'UserController@register');
    $router->add('/register/post', 'UserController@registerPost');

    // Login page (both for the form and for posting)
    $router->add('/login', 'UserController@login');
    $router->add('/login/post', 'UserController@loginPost');

    // Social Media login page
    $router->add('/social', 'SocialMediaController@connectSocialAccount');

    // Logout of the website
    $router->add('/logout', 'UserController@logout');

